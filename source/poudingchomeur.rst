Pouding chômeur
===============================================================================

Litterally "unemployed pudding", or "pudding of the unemployed" (pouding du
chômeur), this is a traditional cake from Québec and Acadie made with cheap
and common ingredients. A lot of variations using maple syrup, chocolate,
apples, and other fancy ingredients can be found on the Internet. This is the
recipe from my grand-mother which uses brown sugar.

.. image:: images/dishes/poudingchomeur.jpg


Ingredients
-------------------------------------------------------------------------------

Syrup

-  245g brown sugar

-  750ml water

-  50g butter

Cake

-  245g brown sugar

-  50g butter

-  1 egg

-  5ml vanilla extract

-  315g flour

-  1 pinch of salt

-  10g baking powder

-  175ml milk



Preparation
-------------------------------------------------------------------------------

1.  Preheat oven at 180° C.

2.  In a medium saucepan, add all the ingredients for the syrup. Bring to a
    boil and simmer for 5 minutes. Pour into an oven proof dish.

3.  In a medium bowl, using an electric mixer, beat together the brown sugar
    and the butter.

4.  Add the egg and vanilla extract and mix well.

5.  In another bowl, mix together the flour, the salt, and the baking powder.

6.  Alternate adding the milk and the flour mix to the wet ingredients and mix
    well. The batter will be thick.

7.  Using a large spoon, drop chunks of the batter in the syrup.

8.  Bake for 25-30 minutes, until a toothpick inserted in the cake comes out
    clean.

9.  Serve warm. You can add cream or vanilla ice cream for an extra decadent
    dessert!


-------------------------------------------------------------------------------

Caroline Cyr La Rose, Québec, Canada

I'm a librarian, knitter, cat lover and ice cream fan from Montreal. I have a
master's degree in Library and information science from the University of
Montréal. I work for Solutions inLibro, inc., a Koha support company located in
Montréal, Québec, Canada. I've been part of the Koha community since 2017 and
I am mainly involved in the documentation team.